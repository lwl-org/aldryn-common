Changelog
=========


Unreleased
----------

1.1.1 (2024-07-06)
------------------

* Replace deprecated Django API ``django.utils.timezone.utc``
* Add ruff config
* Fix logic bug in ``timesince_text`` function


1.1.0 (2024-07-23)
------------------

* Add support for Django 4.0
* Replace usage of ``ugettext``, ``ugettext_lazy`` and ``force_text``


FIXME
-----
